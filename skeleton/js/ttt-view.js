(function () {
  if (typeof TTT === "undefined") {
    window.TTT = {};
  }

  var View = TTT.View = function (game, $el) {
    this.game = game;
    this.$el = $el;
    this.setupBoard();
    this.bindEvents();
  };

  View.prototype.bindEvents = function () {
    this.$el.find("li").on ("click", function(event){
      var $selectedLi = $(event.currentTarget);
      this.makeMove($selectedLi);
    }.bind(this));
  };

  View.prototype.makeMove = function ($square) {
    if (this.game.isOver()) return;
    var currentPlayer = this.game.currentPlayer;
    try {
      this.game.playMove([$square.data("x"), $square.data("y")]);
    } catch (e) {
      if (e instanceof TTT.MoveError) {
        return;
      } else {
        throw e;
      }
    }

    $square.addClass("clicked");
    $square.addClass(currentPlayer);

    if (this.game.isOver()){
      this.$el.find(".winner").text(this.game.winner() + " Wins!!!");
    }

  };

  View.prototype.setupBoard = function () {
    $gameGrid = $("<ul class=\"grid group\"></ul>");
    for (var i = 0; i < 9; i++) {
      var $li = $("<li></li>");
      $li.data("x", i % 3);
      $li.data("y", Math.floor(i / 3));
      $gameGrid.append($li);
    }

    this.$el.find("#game").append($gameGrid);
  };
})();
